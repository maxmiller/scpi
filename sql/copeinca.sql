-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 10-Fev-2017 às 15:45
-- Versão do servidor: 5.5.53-0+deb8u1
-- PHP Version: 5.6.29-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `copeinca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_sistema`
--

CREATE TABLE IF NOT EXISTS `login_sistema` (
`id` int(11) NOT NULL,
  `siape` varchar(7) NOT NULL,
  `data_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `login_sistema`
--

INSERT INTO `login_sistema` (`id`, `siape`, `data_login`) VALUES
(1, '1935921', '2016-03-10 18:17:40'),
(2, '2025504', '2016-03-11 11:11:20'),
(3, '2025504', '2016-03-11 11:15:29'),
(4, '1836860', '2016-03-11 11:15:45'),
(5, '2055569', '2016-03-11 11:16:00'),
(6, '1673019', '2016-03-11 11:22:44'),
(7, '1962001', '2016-03-11 11:29:24'),
(8, '1645707', '2016-03-11 11:46:53'),
(9, '1673019', '2016-03-11 11:57:55'),
(10, '1046185', '2016-03-11 12:05:13'),
(11, '1200329', '2016-03-11 15:12:17'),
(12, '2278916', '2016-03-12 14:39:46'),
(13, '1940119', '2016-03-13 13:51:09'),
(14, '1453548', '2016-03-13 17:42:34'),
(15, '1453548', '2016-03-14 10:22:50'),
(16, '1918984', '2016-03-14 10:23:31'),
(17, '1688264', '2016-03-14 10:23:55'),
(18, '2265824', '2016-03-14 11:35:06'),
(19, '2265824', '2016-03-14 11:44:58'),
(20, '2278916', '2016-03-14 21:08:03'),
(21, '2167061', '2016-03-14 23:23:05'),
(22, '1940119', '2016-03-15 12:58:59'),
(23, '1583768', '2016-03-15 17:32:32'),
(24, '2055569', '2016-03-16 00:56:09'),
(25, '1935921', '2016-03-16 20:42:33'),
(26, '1935921', '2016-03-16 21:06:01'),
(27, '2025504', '2016-03-17 10:10:24'),
(28, '2025504', '2016-03-17 10:44:07'),
(29, '2025504', '2016-03-17 10:48:24'),
(30, '2025504', '2016-03-17 10:57:09'),
(31, '2025504', '2016-03-17 10:57:22'),
(32, '2025504', '2016-03-17 11:35:37'),
(33, '2025504', '2016-03-17 11:40:31'),
(34, '1935921', '2016-03-17 11:47:34'),
(35, '2025504', '2016-03-17 11:49:25'),
(36, '1200329', '2016-03-17 13:15:45'),
(37, '1200329', '2016-03-17 13:21:37'),
(38, '1996988', '2016-03-17 15:55:18'),
(39, '1888388', '2016-03-17 18:11:53'),
(40, '1957598', '2016-03-17 18:27:30'),
(41, '2152373', '2016-03-17 18:35:46'),
(42, '2043886', '2016-03-17 19:23:28'),
(43, '1996988', '2016-03-17 20:03:02'),
(44, '2265824', '2016-03-18 10:12:49'),
(45, '1996988', '2016-03-18 10:27:51'),
(46, '2055569', '2016-03-18 10:43:12'),
(47, '1956341', '2016-03-18 12:34:26'),
(48, '1956341', '2016-03-18 12:43:01'),
(49, '2152373', '2016-03-18 13:20:43'),
(50, '2072657', '2016-03-18 13:32:41'),
(51, '1956341', '2016-03-18 14:41:43'),
(52, '2265824', '2016-03-18 15:15:51'),
(53, '1956341', '2016-03-18 15:17:51'),
(54, '1956341', '2016-03-18 15:30:48'),
(55, '1956341', '2016-03-18 15:31:49'),
(56, '1957598', '2016-03-18 17:17:04'),
(57, '1045649', '2016-03-18 17:49:41'),
(58, '1694728', '2016-03-18 17:52:39'),
(59, '1857698', '2016-03-18 18:59:17'),
(60, '1694728', '2016-03-18 19:09:38'),
(61, '1583768', '2016-03-18 19:10:39'),
(62, '2265824', '2016-03-18 20:32:20'),
(63, '2152373', '2016-03-18 20:41:15'),
(64, '1962001', '2016-03-18 20:46:07'),
(65, '1045649', '2016-03-18 20:47:13'),
(66, '1045649', '2016-03-18 20:55:21'),
(67, '1956341', '2016-03-18 21:52:40'),
(68, '1691794', '2016-03-20 01:11:39'),
(69, '2133235', '2016-03-21 00:02:11'),
(70, '1691794', '2016-03-21 00:16:20'),
(71, '1734433', '2016-03-21 00:17:49'),
(72, '1734433', '2016-03-21 00:21:53'),
(73, '1734433', '2016-03-21 01:31:13'),
(74, '1812228', '2016-03-21 04:16:26'),
(75, '1935921', '2016-03-28 18:25:37'),
(76, '1200329', '2016-03-28 18:31:36'),
(77, '2278916', '2016-03-28 18:32:30'),
(78, '1688264', '2016-03-28 18:32:54'),
(79, '1142335', '2016-03-28 18:32:54'),
(80, '1200329', '2016-03-28 18:34:03'),
(81, '1200329', '2016-03-28 18:34:29'),
(82, '1691794', '2016-03-28 18:41:25'),
(83, '1859860', '2016-03-28 18:54:03'),
(84, '1859860', '2016-03-28 19:06:04'),
(85, '2278916', '2016-03-28 19:06:52'),
(86, '2167061', '2016-03-28 19:10:09'),
(87, '1888388', '2016-03-28 19:13:31'),
(88, '2067162', '2016-03-28 19:18:15'),
(89, '1142335', '2016-03-28 19:20:40'),
(90, '1734433', '2016-03-28 19:42:42'),
(91, '1694426', '2016-03-28 19:43:00'),
(92, '2124992', '2016-03-28 21:01:06'),
(93, '2067162', '2016-03-28 21:15:21'),
(94, '1694426', '2016-03-28 21:21:26'),
(95, '2067162', '2016-03-28 21:23:38'),
(96, '2067162', '2016-03-28 21:23:58'),
(97, '2067162', '2016-03-28 21:24:19'),
(98, '2067162', '2016-03-28 21:27:22'),
(99, '2067162', '2016-03-28 21:31:45'),
(100, '1935921', '2016-03-28 21:33:50'),
(101, '1238084', '2016-03-28 23:11:02'),
(102, '1238084', '2016-03-28 23:23:26'),
(103, '1238084', '2016-03-28 23:26:46'),
(104, '2777919', '2016-03-29 10:31:14'),
(105, '1935921', '2016-03-29 10:37:06'),
(106, '1374554', '2016-03-29 10:38:45'),
(107, '2067162', '2016-03-29 10:56:06'),
(108, '2777919', '2016-03-29 11:02:57'),
(109, '1836860', '2016-03-29 11:12:36'),
(110, '1883948', '2016-03-29 11:17:29'),
(111, '1238084', '2016-03-29 11:20:25'),
(112, '1645707', '2016-03-29 12:22:31'),
(113, '1830990', '2016-03-29 12:31:52'),
(114, '1935921', '2016-03-29 17:25:18'),
(115, '2082756', '2016-03-29 17:39:34'),
(116, '2152373', '2016-03-29 17:51:24'),
(117, '1883948', '2016-03-29 18:27:32'),
(118, '1975528', '2016-03-29 18:42:08'),
(119, '1962001', '2016-03-29 18:55:45'),
(120, '1836860', '2016-03-29 19:16:03'),
(121, '2082756', '2016-03-29 19:16:42'),
(122, '1975528', '2016-03-29 19:52:10'),
(123, '1027699', '2016-03-29 21:21:26'),
(124, '1694426', '2016-03-30 00:03:44'),
(125, '1694426', '2016-03-30 00:47:44'),
(126, '2067162', '2016-03-30 01:08:43'),
(127, '1694426', '2016-03-30 10:22:34'),
(128, '1935921', '2016-03-30 11:11:38'),
(129, '1673019', '2016-03-30 12:00:56'),
(130, '1957598', '2016-03-30 12:03:03'),
(131, '1957598', '2016-03-30 12:03:23'),
(132, '2072657', '2016-03-30 12:04:39'),
(133, '1957598', '2016-03-30 12:04:56'),
(134, '1935921', '2016-03-30 12:11:54'),
(135, '1935921', '2016-03-30 12:11:59'),
(136, '2167061', '2016-03-31 10:17:24'),
(137, '2167061', '2016-03-31 10:19:55'),
(138, '2265824', '2016-03-31 11:14:13'),
(139, '1694426', '2016-03-31 11:17:02'),
(140, '1836860', '2016-03-31 12:04:32'),
(141, '1836860', '2016-03-31 12:05:12'),
(142, '1836860', '2016-03-31 12:07:29'),
(143, '1935921', '2016-03-31 12:09:09'),
(144, '1836860', '2016-03-31 12:16:00'),
(145, '1940119', '2016-03-31 12:19:39'),
(146, '1830990', '2016-03-31 12:21:00'),
(147, '1883948', '2016-03-31 12:21:23'),
(148, '1940119', '2016-03-31 12:23:08'),
(149, '2025504', '2016-03-31 13:06:04'),
(150, '2167061', '2016-03-31 13:07:37'),
(151, '2043886', '2016-04-06 16:38:41'),
(152, '1883948', '2016-04-08 21:44:16'),
(153, '1583768', '2016-04-12 12:34:40'),
(154, '1883948', '2016-04-18 18:59:13'),
(155, '1883948', '2016-06-02 16:45:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `planejamento`
--

CREATE TABLE IF NOT EXISTS `planejamento` (
`id` int(11) NOT NULL,
  `siape` varchar(7) NOT NULL,
  `nome_evento` varchar(2000) NOT NULL,
  `cidade_evento` varchar(1000) NOT NULL,
  `data_inicio_evento` date NOT NULL,
  `data_fim_evento` date NOT NULL,
  `valor_passagem` decimal(10,0) DEFAULT NULL,
  `justificativa_evento_relevancia` varchar(5000) NOT NULL,
  `sitio_evento` varchar(2000) DEFAULT NULL,
  `relevancia_evento` int(11) NOT NULL,
  `projeto_institucional` int(11) NOT NULL,
  `estudando` int(11) NOT NULL,
  `numero_evento_nacional` int(11) NOT NULL,
  `numero_evento_internacional` int(11) NOT NULL,
  `titulacao` int(11) NOT NULL,
  `tempo_servico` int(11) NOT NULL,
  `tipo_evento_capacitacao` int(11) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inscricao` decimal(10,0) DEFAULT '0',
  `total_diarias` decimal(10,0) DEFAULT NULL,
  `total_pontos` decimal(10,0) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `prioridade` int(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `planejamento`
--

INSERT INTO `planejamento` (`id`, `siape`, `nome_evento`, `cidade_evento`, `data_inicio_evento`, `data_fim_evento`, `valor_passagem`, `justificativa_evento_relevancia`, `sitio_evento`, `relevancia_evento`, `projeto_institucional`, `estudando`, `numero_evento_nacional`, `numero_evento_internacional`, `titulacao`, `tempo_servico`, `tipo_evento_capacitacao`, `data_cadastro`, `inscricao`, `total_diarias`, `total_pontos`, `status`, `prioridade`) VALUES
(4, '2055569', 'SimpÃ³sio Brasileiro de Sistemas ElÃ©tricos', 'Natal-RN-Brasil', '2016-05-22', '2016-05-25', 0, '  Durante o evento serÃ¡ apresentado o trabalho "ProteÃ§Ã£o Adaptativa de Sobrecorrente em Sistemas de DistribuiÃ§Ã£o Baseado em Algoritmos GenÃ©ticos" em sessÃ£o oral. ', 'http://2016.sbse.org.br/', 1, 1, 1, 1, 0, 1, 5, 2, '2016-03-11 11:34:19', 700, 735, 82, 0, 0),
(5, '1962001', 'GESTÃO DE PESSOAS NA ADMINISTRAÇÃO PÚBLICA', 'Porto Alegre/RS', '2016-08-24', '2016-08-26', 1074, '  Este curso busca capacitar os servidores públicos para o conhecimento dos principais processos da gestão de pessoas, como fundamentos e conceitos em Gestão de Pessoas no Setor Público, planejamento de RH no setor público, recrutamento e seleção, bem como outros temas de relevante importância', 'http://189.9.128.38/acesso-a-informacao/centros-regionais/rio-grande-do-sul-1/gestao-de-pessoas-na-administracao-publica-1', 1, 0, 0, 1, 0, 1, 4, 7, '2016-03-11 11:34:42', 600, 945, 70, 0, 1),
(7, '1645707', '2 ENAPPE', 'Natal/RN/Brasil', '0000-00-00', '0000-00-00', 400, ' AprovaÃ§Ã£o de trabalho.', 'http://www.2enappe.ce.ufrn.br/enappe/', 1, 1, 1, 1, 0, 0, 9, 2, '2016-03-11 11:53:04', 60, 105, 89, 0, 1),
(8, '1673019', 'GestÃ£o e FiscalizaÃ§Ã£o de Contratos, Conta Vinculada e Encerramento dos Contratos', 'SÃ£o Paulo/SP', '2016-08-09', '2016-08-12', 920, ' Proporcionar amplo conhecimento das normas que regem os contratos administrativos bem como suas peculiaridades, em especial, questÃµes sobre Conta Vinculada e Encerramento de Contratos.', 'http://www.consultre.com.br', 0, 0, 0, 0, 0, 1, 14, 7, '2016-03-11 12:09:15', 0, 315, 65, 0, 0),
(9, '1453548', 'CARLOS EUGÃŠNIO DE FARIA', 'SAO LUIZ / MARANHÃƒO / BRASIL', '2016-07-24', '2016-07-30', 1200, ' Ã‰ um evento que reÃºne os mais importantes geÃ³grafos do  Brasil e do mundo e discute o que hÃ¡ de mais atual na Geografia....', 'www.eng.com.br', 1, 0, 0, 0, 0, 1, 6, 3, '2016-03-13 18:06:02', 0, 1365, 68, 0, 0),
(10, '1688264', 'FEBRATEX', 'Blumenau / Santa Catarina', '2016-08-09', '2016-08-12', 0, ' Ãˆ a maior feira da America do Sul quanto as novas tecnologias da cadeia tÃªxtil , confecÃ§Ãµes e Moda. Teremos contatos jÃ¡ agendado junto as empresas para parcerias com nossa instituiÃ§Ã£o quanto a doaÃ§Ãµes de equipamentos, produtos e tecidos especiais .', 'www.febratex.com.br', 1, 0, 0, 4, 0, 0, 36, 1, '2016-03-14 10:32:20', 0, 3500, 73, 0, 0),
(11, '1918984', 'Febratex - Feira Brasileira para a IndÃºstria TÃªxtil', 'Blumenau/Santa Catarina/Brasil', '2016-08-09', '2016-08-12', 1500, 'Evento TÃªxtil de maior relevÃ¢ncia no Brasil, acontecendo de 2 em 2 anos, com o que existe de mais moderno na Ã¡rea  tÃªxtil. AlÃ©m dos equipamentos apresenta palestras com assuntos ligados Ã  Ã¡rea\r\nComo tÃ©cnico de laboratÃ³rio e participando da elaboraÃ§Ã£o de especificaÃ§Ã£o para aquisiÃ§Ã£o de equipamentos para os nossos laboratÃ³rios, preciso me manter atualizado.\r\n', 'http://www.febratex.com.br', 1, 1, 1, 2, 0, 1, 49, 3, '2016-03-14 10:33:40', 0, 735, 78, 0, 0),
(12, '2265824', 'FEBRATEX', 'Blumenau/ Santa Catarina / Brasil', '2016-08-09', '2016-08-12', 950, '        Ã‰ a maior feira da AmÃ©rica do Sul para o mercado tÃªxtil. Ã‰ apresentado todas as novidades em equipamentos, produtos quÃ­micos e processos ligados a indÃºstria tÃªxtil. Possui palestras sobre  novas tecnologias e pesquisas de mercado. Ã‰ uma excelente oportunidade de atualizaÃ§Ã£o de conhecimentos da Ã¡rea tÃªxtil.       ', 'www.febratex.com.br', 1, 0, 0, 0, 0, 0, 113, 3, '2016-03-14 11:44:02', 0, 735, 88, 0, 0),
(13, '2278916', '3º Congresso Internacional de Moda e Design', 'Buenos Aires / Argentina', '2016-05-09', '2016-05-12', 1374, '  Artigo aprovado para apresentaÃ§Ã£o oral, alÃ©m de ser o maior evento cientÃ­fico na Ã¡rea de interesse (moda).', 'http://www.design.uminho.pt/cimode2016/pt-PT/', 1, 0, 0, 0, 0, 1, 1, 4, '2016-03-14 21:13:04', 0, 735, 51, 0, 0),
(14, '2167061', 'VII Congresso Paraibano de Odontologia', 'JoÃ£o Pessoa / PB / Brasil', '2016-08-25', '2016-08-27', 0, '      A participaÃ§Ã£o nesse evento justifica-se diante da necessidade de aperfeiÃ§oamento constante, bem como ser uma oportunidade importante para fomentar novos conhecimentos em minha Ã¡rea de atuaÃ§Ã£o. ', '', 1, 0, 0, 1, 0, 1, 3, 2, '2016-03-14 23:45:47', 400, 525, 52, 0, 1),
(15, '1940119', 'FEBRATEX - Feira Brasileira para a IndÃºstria TÃªxtil', 'Blumenau - Sc', '2016-08-09', '2016-08-12', 1100, ' Evento realizado a cada 2 anos, onde se reuni as principais industrias, fornecedores, marcas da industria tÃªxtil e de vestuÃ¡rio.', 'http://www.febratex.com.br/inicio', 1, 0, 0, 0, 0, 1, 4, 1, '2016-03-15 13:02:14', 0, 3500, 65, 0, 0),
(16, '1935921', 'CSBC 2016', 'Florianopolis - SC', '2016-07-04', '2016-07-07', 1200, '     O maior evento da área da computação, reunindo mais de 10 subeventos! Incluindo o 3° ENCompIF - Encontro Nacional de Computação dos Institutos Federais ', 'http://www.csbc2016.com.br/', 1, 1, 0, 0, 1, 1, 7, 7, '2016-03-16 20:48:23', 240, 735, 81, 0, 1),
(19, '2025504', 'CONGRESSO BRASILEIRO DE ASSISTENTES SOCIAIS', 'RECIFE', '2016-09-05', '2016-09-09', 0, '   O evento Ã© o mais importante da categoria de assistentes sociais e sÃ³ acontece a cada trÃªs anos. Promove a capacitaÃ§Ã£o/aperfeiÃ§oamento da prÃ¡tica profissional por meio de palestras, oficinas e minicursos escolhidos no ato da inscriÃ§Ã£o. ', 'http://www.cbas2016.com.br/', 1, 0, 0, 2, 0, 1, 6, 2, '2016-03-17 11:52:17', 600, 945, 69, 0, 0),
(21, '1200329', '3º CIMODE - Congresso Internacional de Moda e Design', 'Buenos Aires/Argentina', '2016-05-09', '2016-05-12', 1454, ' Artigo aceito para comunicação oral. ', 'http://www.design.uminho.pt/cimode2016/pt-PT/default.rhtml', 1, 0, 0, 0, 0, 1, 1, 4, '2016-03-17 13:21:04', 0, 315, 64, 0, 0),
(22, '1996988', 'Congresso Brasileiro de QuÃ­mica -CBQ', 'BelÃ©m/PA', '2016-11-07', '2016-11-11', 900, '  ApresentaÃ§Ã£o de trabalho referente aos resultados obtidos no projeto de extensÃ£o desenvolvido no segundo semestre de 2015. ', 'http://www.abq.org.br/cbq/index.html', 0, 0, 0, 2, 0, 2, 1128, 3, '2016-03-17 16:21:50', 486, 945, 58, 0, 0),
(23, '1888388', 'ElaboraÃ§Ã£o de Projeto BÃ¡sico e Termo de ReferÃªncia', 'Natal/RN', '2016-05-31', '2016-06-03', 0, ' Com o objetivo de tornar mais eficiente o sistema de licitaÃ§Ã£o no Campus, diminuindo a necessidade em realizar carona em pregÃµes que nÃ£o atendem nossa demanda especÃ­fica. ', 'http://www.consultre.com.br/curso/elaboracao-do-projeto-basico-e-termo-de-referencia', 0, 0, 0, 1, 0, 1, 9, 8, '2016-03-17 18:19:45', 2490, 735, 63, 0, 0),
(24, '2072657', 'Curso Completo: LicitaÃ§Ãµes e contratos, Registro de PreÃ§o e PregÃ£o', 'Natal RN', '2016-11-21', '2016-11-25', 0, ' AperfeiÃ§oamento na Ã¡rea de trabalho', '', 0, 0, 0, 0, 0, 1, 1, 3, '2016-03-18 13:38:03', 2980, 945, 56, 0, 0),
(25, '1956341', 'Curso de SaÃºde e SeguranÃ§a no Trabalho', 'Natal/RN/BR', '2016-04-04', '2016-04-10', 0, '  Conhecer os riscos de acidentes e doenÃ§as ocupacionais e estratÃ©gias preventivas para minimizar os riscos ambientais no trabalho e circunvizinhanÃ§a Ã© algo que todo cidadÃ£o deveria saber. Assim, a capacitaÃ§Ã£o Ã© o pontapÃ© inicial para esses conhecimentos. O curso seÅ•a realizado em dias quebrados sendo 04/04,05/04,11/04,12/04,18/04,19/04', 'https://webmail.ifrn.edu.br/owa/#path=/mail', 1, 0, 0, 2, 0, 0, 7, 0, '2016-03-18 15:29:49', 0, 1365, 83, 0, 0),
(26, '1957598', 'CONTABILIDADE PÃšBLICA', 'FORTALEZA/CE/BRASIL', '2016-07-24', '2016-07-28', 0, '   O curso Ã© relevante porque visa o aperfeiÃ§oamento das atividades que vÃªm sendo desenvolvidas no setor. ', 'http://www.esafi.com.br/', 1, 1, 1, 1, 0, 1, 5, 2, '2016-03-18 17:24:32', 2490, 105, 72, 0, 0),
(28, '1045649', 'Jean Carlos Dias Ferreira', 'Campinas-SP', '2016-06-29', '2016-07-01', 800, 'ApresentaÃ§Ã£o de um projeto de extensÃ£o realizado aqui na instituiÃ§Ã£o com respeito a temÃ¡tica do evento. ', 'anpoll.org.br', 1, 0, 1, 0, 0, 1, 1, 3, '2016-03-18 18:04:29', 200, 525, 83, 0, 0),
(29, '1694728', 'FEBRATEX', 'Blumenal-SC', '2016-08-09', '2016-08-12', 527, '  ParticipaÃ§Ã£o da Feira Brasileira para a IndÃºstria TÃªxtil. Visualizar novos equipamentos de empresas fabricantes, palestras de profissionais de varios segmentos da cadeia tÃªxtil e de manufatura de uma forma geral. ', 'http://www.febratex.com.br/inicio', 1, 0, 0, 2, 0, 1, 6, 8, '2016-03-18 18:06:35', 564, 735, 63, 0, 0),
(30, '1045649', 'Jean Carlos Dias Ferreira', 'FlorianÃ³polis-SC', '2016-11-28', '2016-12-02', 1200, ' ApresentaÃ§Ã£o de 02 trabalhos cientÃ­ficos realizados na instituiÃ§Ã£o IFRN Campus/CaicÃ³', 'www.congressotils.com.br', 1, 0, 1, 0, 0, 1, 1, 3, '2016-03-18 18:20:46', 300, 945, 83, 0, 0),
(31, '1857698', 'FEBRATEX - Feira brasileira para industria tÃªxtil', 'Blumenau - SC', '2016-08-09', '2016-08-12', 1409, ' Estreitar as relaÃ§Ãµes entre escola e empresas atravÃ©s de contatos com as empresas que participam da feira. E conhecer novos equipamentos que estÃ£o no mercado. ', 'http://www.febratex.com.br/inicio', 1, 1, 0, 1, 0, 1, 4, 3, '2016-03-18 19:08:56', 0, 735, 68, 0, 0),
(32, '2152373', 'Cerimonial, protocolo e organização de eventos solenes públicos', 'Recife/PE/Brasil', '2016-08-24', '2016-08-26', 366, '   Como Coordenadora de Comunicação Social e Eventos, sou responsável pelo planejamento dos eventos solenes no campus, como as formaturas, e vez que  não tenho formação nessa área, necessito de capacitação para desempenhar essa função do modo mais satisfatório possível. ', 'http://www.consultre.me/orgeventos', 0, 0, 0, 0, 0, 1, 3, 7, '2016-03-18 20:43:08', 2490, 525, 63, 0, 1),
(33, '1045649', 'Jean Carlos Dias Ferreira', 'BrasÃ­lia-DF', '2016-09-14', '2016-09-16', 1000, '  ApresentaÃ§Ã£o do projeto de extensÃ£o do NAPNE Campus/CaicÃ³ no evento Nacional dos NAPNE''S', 'Em construÃ§Ã£o', 1, 0, 1, 0, 0, 1, 1, 3, '2016-03-18 20:56:23', 150, 525, 83, 0, 0),
(34, '1045649', 'Jean Carlos Dias Ferreira', 'Fortaleza-CE', '2016-07-19', '2016-07-22', 0, 'ParticipaÃ§Ã£o do evento para agregar conhecimentos e valores na Ã¡rea de LIBRAS. ', '1eventosels.wix.com', 1, 0, 0, 0, 0, 1, 1, 2, '2016-03-18 21:01:01', 90, 735, 73, 0, 0),
(35, '2133235', 'I encontro de EducaÃ§Ã£o FÃ­sica do IFRN', 'Paranamirim RN', '2016-06-03', '2016-06-05', 0, ' Evento realizado pelo NCE de EducaÃ§Ã£o FÃ­sica do IFRN que objetivarÃ¡ expandir as discussÃµes sobre as diferentes metodologias aplicadas a EducaÃ§Ã£o FÃ­sica escolar, e assim, contribuirÃ¡ diretamente para o fazer pedagÃ³gico em nosso campus.', '', 1, 0, 0, 0, 0, 1, 4, 7, '2016-03-21 00:06:31', 0, 525, 75, 0, 0),
(36, '1734433', 'IV SEMINÃRIO NACIONAL DO ENSINO MÃ‰DIO - Ensino, Juventude e Diversidade na Escola PÃºblica / I ENCONTRO NACIONAL ENSINO E INTERDISCIPLINARIDADE NA ESCOLA PÃšBLICA', 'MossorÃ³/ RN/ Brasil', '0000-00-00', '0000-00-00', 0, ' â€œEste evento tem como propÃ³sito constituir-se  como um  espaÃ§o de discussÃµes e debates para os pesquisadores, os estudantes da graduaÃ§Ã£o e da pÃ³s-graduaÃ§Ã£o, os professores universitÃ¡rios e da educaÃ§Ã£o bÃ¡sica que atuam, pesquisam, refletem e problematizam o ensino mÃ©dio (agora ampliando, com o ENACEI, para todas as etapas do ensino pÃºblico), sendo este considerado lÃ³cus privilegiado de produÃ§Ã£o e apropriaÃ§Ã£o de conhecimentos e desenvolvimento de prÃ¡ticas educativas inovadoras. AlÃ©m disso, tais eventos pretendem, ainda, oportunizar um espaÃ§o de socializaÃ§Ã£o e publicizaÃ§Ã£o das pesquisas e produÃ§Ãµes recentes dos sujeitos pedagÃ³gicos (docentes e discentes), visando apontar  o percurso histÃ³rico, a atualidade, as percepÃ§Ãµes e as tranformaÃ§Ãµes  teÃ³ricas e prÃ¡ticas que envolvem a educaÃ§Ã£o e o ensino na escola pÃºblica.â€ Desse modo, constitui-se um evento relevante para nÃ³s, uma vez que publicaremos resultados oriundos do projeto de pesquisa â€œPrÃ¡ticas de Letramento Digital: o blog como estratÃ©gia pedagÃ³gica em aulas de lÃ­ngua inglesaâ€ realizado no campus CaicÃ³.\r\n\r\n', 'http://uern.br/eventos/senacem/default.asp?item=senacem-inicio', 1, 0, 1, 0, 1, 1, 2, 2, '2016-03-21 00:31:48', 50, 315, 69, 0, 0),
(37, '1734433', 'VI CLAFPL â€“ Congresso Latino-Americano de FormaÃ§Ã£o de Professores de LÃ­nguas', 'Londrina/PR/Brasil', '0000-00-00', '0000-00-00', 1700, ' O evento serÃ¡ constituÃ­do por conferÃªncias, mesas-redondas, simpÃ³sios, sessÃµes de comunicaÃ§Ãµes coordenadas e individuais, alÃ©m de sessÃµes de pÃ´steres, que contarÃ£o com nomes representativos da Ã¡rea de formaÃ§Ã£o de professores do Brasil e do exterior. Assim, eventos como este oportunizam a nÃ³s, professores-pesquisadores, um espaÃ§o de socializaÃ§Ã£o e publicizaÃ§Ã£o das pesquisas e produÃ§Ãµes recentes, alÃ©m do conhecimento de outras pesquisas e experiÃªncias em sala de aula. ', 'http://www.viclafpl.com.br/pt/apresentacao/', 1, 0, 1, 0, 1, 1, 2, 1, '2016-03-21 00:50:03', 150, 500, 70, 0, 0),
(38, '1734433', 'International Conference on Information, Communication Technologies in Education', 'Rhodes/ GrÃ©cia', '0000-00-00', '0000-00-00', 2800, ' O presente congresso objetiva apresentar as tecnolÃ³gicas aplicadas na educaÃ§Ã£o, visando o compartilhamento de experiÃªncias Ã  nÃ­vel internacional das TICs em sala de aula. Desse modo, constitui-se um evento relevante para nÃ³s, uma vez que publicaremos resultados oriundos do projeto de pesquisa â€œPrÃ¡ticas de Letramento Digital: o blog como estratÃ©gia pedagÃ³gica em aulas de lÃ­ngua inglesaâ€ realizado no campus CaicÃ³.', 'http://www.icicte.org/', 1, 0, 1, 0, 1, 1, 2, 5, '2016-03-21 01:11:44', 1500, 525, 64, 0, 0),
(39, '1734433', 'III CONGRESSO NACIONAL DE EDUCAÃ‡ÃƒO (CONEDU)', 'NATAL/RN/BRASIL', '0000-00-00', '0000-00-00', 0, ' Eventos como este oportunizam a nÃ³s, professores-pesquisadores, um espaÃ§o de socializaÃ§Ã£o e publicizaÃ§Ã£o das pesquisas e produÃ§Ãµes recentes na Ã¡rea de EducaÃ§Ã£o, alÃ©m do conhecimento de outras pesquisas e experiÃªncias em sala de aula. ', 'http://www.conedu.com.br/index.php', 1, 0, 1, 0, 1, 1, 2, 2, '2016-03-21 01:23:03', 95, 315, 69, 0, 0),
(40, '1734433', '4Âº COLÃ“QUIO INTERNACIONAL DE ESTUDOS LINGUÃSTICOS E LITERÃRIOS â€“ CIELLI', 'MaringÃ¡/PR/Brasil', '0000-00-00', '0000-00-00', 1010, ' Eventos como este oportunizam a nÃ³s, professores-pesquisadores, um espaÃ§o de socializaÃ§Ã£o e publicizaÃ§Ã£o das pesquisas e produÃ§Ãµes recentes na Ã¡rea de EducaÃ§Ã£o, alÃ©m do conhecimento de outras pesquisas e experiÃªncias em sala de aula. ', 'http://cielli2016.blogspot.com.br/p/blog-page.html', 1, 0, 1, 0, 1, 1, 2, 1, '2016-03-21 01:30:36', 150, 1500, 70, 0, 0),
(42, '1374554', 'IV COLÓQUIO DE EDUCAÇÃO PROFISSIONAL', 'Natal -RN/BRASIL', '2016-08-04', '2016-08-07', 0, ' aprofundar os estudos acerca da educação profissional', 'http://portal.ead.ifrn.edu.br/coloquio', 1, 1, 0, 0, 0, 1, 14, 2, '2016-03-29 10:43:47', 120, NULL, NULL, NULL, 10),
(43, '2067162', 'Connepi 2016', 'Brasilia/DF/Brasil', '2016-11-30', '2016-12-03', 2400, '  Evento de divulgação cientifica com apresentação de trabalhos do campus Caicó buscando o intercambio da produção cientifica da area de educação física na educa integrada.', '', 1, 1, 1, 1, 0, 1, 4, 0, '2016-03-29 10:59:16', 0, NULL, NULL, NULL, 1),
(44, '2777919', 'JOSÉ CARLOS VIEIRA DE SOUZA', 'NATAL-RN', '2016-08-17', '2016-08-20', 0, '  A cada dia,  fazemos uso de softwares, para dinamizar as aulas e melhorar aprendizagem dos conteúdos ensinados. O Geogebra é um desses, que possibilita tal ação. O I Congresso Brasileiro do Geogebra: múltiplos olhares para o ensino e aprendizagem de conceitos, será uma excelente oportunidade de CAPACITAÇÃO com intuito de tornar as aulas de matemáticas dos alunos do IFRN - Caicó mais atrativas.\r\n', 'http://geogebra.hol.es/', 1, 0, 0, 1, 0, 0, 7, 8, '2016-03-29 10:59:16', 0, NULL, NULL, NULL, 1),
(45, '1238084', 'Congresso Brasileiro de Automótica', 'Vitória/Espírito Santo/Brasil', '2016-10-03', '2016-10-07', 1600, ' Apresentar artigo caso esse seja aprovado ', 'http://cba2016.org.br/', 1, 1, 1, 0, 0, 1, 0, 3, '2016-03-29 11:29:23', 600, NULL, NULL, NULL, 1),
(46, '1830990', 'XXXVI Congresso da Sociedade Braliseira de Computação', 'Porta Alegre/RS/Brasil', '2016-07-04', '2016-07-07', 1206, ' O CSBC 2016 promoverá espaços para trocas, discussões e construções na comunidade, de maneira a aumentar a sinergia na busca de soluções efetivamente interdisciplinares para os complexos problemas que emergem na sociedade atual. Entre os 18 eventos que irão ocorre dentro do CSBC podemos destacar o III Encontro Nacional de Computação dos Institutos Federais (ENCompIF), que tem por objetivo reunir professores e alunos de ensino médio dos Institutos Federais e CEFETs de todo o país. Onde serão apresentados trabalhos realizados nos Institutos Federais e Cefets, mostrando-se como uma excelente oportunidade para a troca de experiências entre os participantes e um estímulo a integração e estabelecimento de parcerias entre professores e alunos e diferentes Institutos Federais e CEFETs.', 'http://www.csbc2016.com.br/', 1, 1, 1, 1, 0, 1, 12, 3, '2016-03-29 12:45:51', 220, NULL, NULL, NULL, 1),
(47, '2152373', 'Oficina de Texto Jornalístico', 'Natal/RN/Brasil', '2016-09-12', '2016-09-16', 0, ' No desempenho das minhas atribuições na Comunicação Social e de Eventos do Campus escrevo textos de cunho jornalístico para o site institucional, portanto é essencial conhecer as técnicas específicas deste tipo de produção textual para aprimorar as publicações do Campus, vez que não tenho formação na área de jornalismo. Essa capacitação está prevista para ser promovida no 2º semestre de 2016 pela Assessoria de Comunicação Social e de Eventos (ASCE) [as datas de início e fim são uma estimativa]', '', 1, 0, 0, 0, 0, 1, 3, 7, '2016-03-29 18:06:48', 0, NULL, NULL, NULL, 2),
(48, '2152373', 'Oficina de Fotografia', 'Natal/RN/Brasil', '2016-01-17', '2016-10-21', 0, '  Uma das atribuições da Comunicação Social e de Eventos do Campus é realizar o registro fotográfico de eventos promovidos pelo Campus ou daqueles nos quais o Campus participe para posterior divulgação no site institucional. Dessa forma, estudar  a teoria para conhecer e poder usar vários efeitos e técnicas que tornem as fotografias mais profissionais trará maior qualidade ao trabalho de divulgação das ações do Campus. Essa capacitação está prevista para ser promovida no 2º semestre de 2016 pela Assessoria de Comunicação Social e de Eventos (ASCE) [as datas de início e fim são uma estimativa].', '', 1, 0, 0, 0, 0, 1, 3, 7, '2016-03-29 18:15:33', 0, NULL, NULL, NULL, 3),
(49, '1883948', ' Congresso da Sociedade Brasileira de Computação 2016 - Computação e Interdisciplinaridade', 'Porto Alegre/RS/Brasil', '2016-07-04', '2016-07-07', 1796, '    O CSBC 2016, será composto por um conjuntos de eventos de várias área da computação que irá promover espaços para trocas, discussões, capacitações e construções na comunidade de Informática, de maneira a aumentar a sinergia na busca de soluções efetivamente interdisciplinares para soluções no dia a dia da computação em todos os segmentos do desenvolvimento da sociedade. \r\n', 'http://www.csbc2016.com.br/', 1, 1, 0, 0, 0, 1, 9, 3, '2016-03-29 18:46:19', 640, NULL, NULL, NULL, 1),
(50, '1975528', 'GESTÃO DE PESSOAS NA ADMINISTRAÇÃO PÚBLICA', 'PORTO ALEGRE - RS', '0000-00-00', '0000-00-00', 1074, 'O curso visa capacitar os servidores públicos para o conhecimento dos principais processos da gestão de pessoas. \r\nO Conteúdo programático é bastante completo e a participação das servidoras lotadas no setor de recursos humanos nos auxiliará a desenvolver melhor nossas atribuições, principalmente no que diz respeito a condução do Projeto de Qualidade de Vida, Motivação, Gerenciamento dos conflitos e desenvolvimento de pessoas.  \r\nConteúdo programático: \r\n1. GESTÃO DE PESSOAS NO SERVIÇO PÚBLICO a) Fundamentos e conceitos em Gestão de Pessoas no Setor Público. b) Gestão de Pessoas e Reforma do Estado. c) Planejamento de RH no setor público. d) Recrutamento e Seleção.\r\n2. RELAÇÕES E FATORES HUMANOS a) Motivação Humana. b) Liderança e Poder. c) As Culturas nas organizações. d) Clima organizacional. e) Mudança organizacional.\r\n3. ÉTICA E CONFLITOS NAS RELAÇÕES DE TRABALHO a) Ética b) Gestão de Conflitos e Negociação\r\n4. DESENVOLVER PESSOAS a) Noção de Competência b) Desenvolvimento e Treinamento c) Avaliação da GP no Governo\r\n5. TENDÊNCIAS E DESAFIOS a) Qualidade de Vida no Trabalho (QVT) b) Diagnóstico de Governança e Gestão de Pessoas na Administração Pública Brasileira.', 'http://189.9.128.38/acesso-a-informacao/centros-regionais/rio-grande-do-sul-1/gestao-de-pessoas-na-administracao-publica-1', 1, 0, 0, 0, 0, 0, 3, 7, '2016-03-29 19:14:15', 600, NULL, NULL, NULL, 1),
(51, '1836860', 'Congresso Nacional de Gestão e Fiscalização de Contratos de Obras e Serviços de Engenharia', 'NATAL / RN / BRASIL', '0000-00-00', '0000-00-00', 0, ' Aprimorar os conhecimentos na área de gestão e fiscalização de contratos.', 'http://institutoideha.com.br/cursos/iv-congresso-nacional-de-gestao-e-fiscalizacao-de-contratos-de-obras-e-servicos-de-engenharia-98', 1, 0, 0, 0, 0, 1, 10, 7, '2016-03-29 19:29:15', 2890, NULL, NULL, NULL, 1),
(52, '2082756', 'XI ANPED SUL', 'Curitiba', '2016-07-24', '2016-07-27', 700, '  o encontro tem como tema Educação, movimentos sociais e políticas governamentais, e a inspiração da poesia de Paulo Leminski. Essa temática é  relevante para a sistematização do meu trabalho na ETEP e no NEABI, Dessa forma, participar das reuniões e discussões dos pesquisadores irá contribuir tanto para minha formação continuada quanto para minha prática profissional.', 'http://www.anpedsul2016.ufpr.br/apresentacao/', 1, 1, 0, 0, 0, 1, 5, 0, '2016-03-29 19:54:56', 265, NULL, NULL, NULL, 1),
(53, '1975528', 'Gestão de Pessoas: fundamentos e tendências', 'Brasilia/DF', '0000-00-00', '0000-00-00', 921, '  O curso é voltado para servidores públicos do poder executivo que trabalhem na área de Gestão de Pessoas. \r\nPrincipais Tópicos:  Noções sobre Gestão de Pessoas;  Política de Gestão de Pessoas na Administração Pública Federal; Sistemas, pressupostos, problemas e desafios da gestão de pessoas na área pública; Contexto histórico: aspectos do modelo de gestão de pessoas na Administração Pública Federal (APF);  Gestão Estratégica de Pessoas;  Mecanismos e instrumentos da gestão estratégica de pessoas;  Mudanças no mundo do trabalho: Perfil do Servidor e a nova área de Gestão de Pessoas;A noção de competência; Plano Nacional de Desenvolvimento de Pessoas - PNDP;   A aplicação e integração do modelo de gestão de pessoas nos processos de planejamento de força de trabalho, recrutamento e seleção de pessoas, capacitação, remuneração e gestão do desempenho;  Relações entre estratégia organizacional, capacitação e desenvolvimento profissional e desempenho;  Gestão do Conhecimento;\r\nCultura Organizacional;  Clima Organizacional; Motivação; Ética; Criatividade e Inovação;  Gestão da Diversidade;  Qualidade de Vida no Trabalho - QVT;  Relatório OCDE: Brasil 2010; Diagnóstico de Governança e Gestão de Pessoas na Administração Pública Brasileira - TCU 2013.', 'http://www.enap.gov.br/web/pt-br/sobre-curso?p_p_id=enapvisualizardetalhescurso_WAR_enapinformacoescursosportlet&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_r_p_564233524_idCurso=2476', 1, 0, 0, 0, 0, 0, 3, 7, '2016-03-29 20:23:14', 0, NULL, NULL, NULL, 1),
(54, '1694426', 'XI Seminario de la Red ESTRADO Movimientos Pedagógicos y Trabajo Docente en tiempos de estandarización', 'Cidade do México/México', '2016-03-15', '2016-03-19', 3000, '    O evento consiste na análise de políticas educacionais e da formação docente, discutindo pesquisas e relatos de experiências numa perspectiva interdisciplinar e transdisciplinar da prática educativa. Como membro da Equipe técnico-pedagógica participar do evento implica em refletir sobre o papel do profissional em educação na busca de materializar a função social do IFRN na educação profissional do século XXI. Além de corroborar com a articulação de experiências curriculares voltadas para formação cidadã e planterária mediante as discussões iminentes para a busca da qualidade da educação num processo contínuo, coletivo, democrático e, essencialmente, humano.', 'http://www.redeestrado.org/web/4/1.php?idioma=port', 1, 1, 0, 2, 0, 1, 8, 5, '2016-03-30 00:49:58', 200, NULL, NULL, NULL, 1),
(55, '2067162', 'I ENCONTRO DE EDUCAÇÃO FÍSICA DO IFRN', 'Parnamirim/RN/Brasil', '2016-06-01', '2016-06-03', 0, ' Socialização do conhecimento e metodologia de trabalho aplicado no IFRN em se tratando de Educação Física e Qualidade de Vida no Trabalho.', '', 1, 1, 1, 1, 0, 1, 4, 0, '2016-03-30 01:12:09', 0, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servidor`
--

CREATE TABLE IF NOT EXISTS `servidor` (
  `siape` varchar(7) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servidor`
--

INSERT INTO `servidor` (`siape`, `nome`, `email`) VALUES
('1027699', 'Angela Lima Calou', 'angela.calou@ifrn.edu.br'),
('1034552', 'Jose Renato Pereira Brasil', 'brasil.renato@ifrn.edu.br'),
('1045649', 'Jean Carlos Dias Ferreira', 'jean.ferreira@ifrn.edu.br'),
('1046185', 'Alexandro Diogenes Barreto', 'alexandro.barreto@ifrn.edu.br'),
('1047828', 'Romerito Campos de Andrade', 'romerito.campos@ifrn.edu.br'),
('1049745', 'Francisca Jucileuda da Silva Sousa', 'sousa.jucileuda@ifrn.edu.br'),
('1088356', 'Rhodriggo Mendes Virginio', 'rhodriggo.virginio@ifrn.edu.br'),
('1142335', 'Larissa Fernanda dos Santos Oliveira', 'larissa.oliveira@ifrn.edu.br'),
('1200329', 'Layla de Brito Mendes', 'layla.mendes@ifrn.edu.br'),
('1238084', 'Daniel Enos Cavalcanti Rodrigues de Macedo', 'daniel.macedo@ifrn.edu.br'),
('1374554', 'Debora Suzane de Araujo Faria', 'debora.faria@ifrn.edu.br'),
('1451459', 'Ricardo Augusto Pereira', 'ricardo.pereira@ifrn.edu.br'),
('1453548', 'Carlos Eugenio de Faria', 'carlos.faria@ifrn.edu.br'),
('1530568', 'Cleiton da Silva Medeiros', 'cleiton.medeiros@ifrn.edu.br'),
('1556707', 'Luciane Soares Almeida', 'luciane.almeida@ifrn.edu.br'),
('1583768', 'Samir de Carvalho osta', 'samir.costa@ifrn.edu.br'),
('1586466', 'Joao Paulo de Medeiros Santos', 'joao.santos@ifrn.edu.br'),
('1614375', 'Ermerson de Oliveira C apistrano', 'ermerson.capistrano@ifrn.edu.br'),
('1645707', 'Damiao Paulo da Silva Filho', 'damiao.filho@ifrn.edu.br'),
('1672843', 'Tales Vinicio Fernandes Vale', 'tales.vale@ifrn.edu.br'),
('1672955', 'Cynara Tercia Martins Borges', 'cynara.borges@ifrn.edu.br'),
('1673019', 'Maria das Vitorias de Macedo Azevedo', 'maria.azevedo@ifrn.edu.br'),
('1677639', 'Carlos Helaidio Chaves da Costa', 'carlos.chaves@ifrn.edu.br'),
('1680070', 'Jarbas Medeiros de Lima Filho', 'jarbas.medeiros@ifrn.edu.br'),
('1687390', 'Rosa Maria da Silva Medeiros', 'rosa.medeiros@ifrn.edu.br'),
('1688264', 'Edson Caetano Bottini', 'edson.bottini@ifrn.edu.br'),
('1691794', 'Joseclebio da Fonseca Lucena', 'fonseca.lucena@ifrn.edu.br'),
('1694426', 'Suely Soares da Nobrega', 'suely.nobrega@ifrn.edu.br'),
('1694728', 'Jose Henrique Batista Lima', 'henrique.batista@ifrn.edu.br'),
('1696345', 'Joao Paulo Pereira de Araujo', 'joao.araujo@ifrn.edu.br'),
('1705213', 'Marcos Batista de Souza', 'marcos.batista@ifrn.edu.br'),
('1705284', 'Joao Victor Alves Feitoza', 'joao.feitoza@ifrn.edu.br'),
('1721622', 'Maria Jose de Oliveira', 'maria.oliveira@ifrn.edu.br'),
('1721787', 'Joaildo Maia', 'joaildo.maia@ifrn.edu.br'),
('1734020', 'Haulisson Jody Batista da Costa', 'haulisson.costa@ifrn.edu.br'),
('1734433', 'Mirelly Karolinny de Melo Meireles', 'mirelly.meireles@ifrn.edu.br'),
('1743533', 'Jose Kleber Costa de Oliveira', 'jose.kleber@ifrn.edu.br'),
('1755897', 'Italo Batista da Silva', 'italo.batista@ifrn.edu.br'),
('1782507', 'Ricardo Rodrigues da Silva', 'ricardo.rodrigues@ifrn.edu.br'),
('1812228', 'Giancarlos Costa Barbosa', 'giancarlos.barbosa@ifrn.edu.br'),
('1814194', 'Alisson Diego Dias de Medeiros', 'alisson.dias@ifrn.edu.br'),
('1823238', 'Sandra Maria de Assis', 'sandra.assis@ifrn.edu.br'),
('1826050', 'Luiz Carlos Medeiros de Oliveira', 'luiz.oliveira@ifrn.edu.br'),
('1830990', 'Alessandro Vinicius Pereira Rolim de Araujo', 'alessandro.rolim@ifrn.edu.br'),
('1832967', 'Samuel Santos Simoes', 'samuel.simoes@ifrn.edu.br'),
('1836728', 'Maria das Dores da Rocha Medeiros', 'dores.rocha@ifrn.edu.br'),
('1836860', 'Ary Torres de Araujo Neto', 'ary.torres@ifrn.edu.br'),
('1855961', 'Matheus Mazukyewsky Oliveira de Medeiros', 'matheus.oliveira@ifrn.edu.br'),
('1857698', 'Rubens Capistrano de Araujo', 'rubens.capistrano@ifrn.edu.br'),
('1857755', 'Moally Janne de Brito Soares', 'moally.soares@ifrn.edu.br'),
('1859860', 'Tatiana Ribeiro Ferreira', 'tatiana.ferreira@ifrn.edu.br'),
('1863122', 'Sandra Regia Ferreira', 'sandra.ferreira@ifrn.edu.br'),
('1883737', 'Tiago de Lima Wanderley', 'tiago.wanderley@ifrn.edu.br'),
('1883948', 'Geam Carlos de Araujo Filgueira', 'geam.filgueira@ifrn.edu.br'),
('1886850', 'Katia Simonne Oliveira Dias', 'katia.dias@ifrn.edu.br'),
('1888129', 'Gleydson Giovanni Simplicio de Oliveira', 'gleydson.simplicio@ifrn.edu.br'),
('1888388', 'Maria do Socorro de Oliveira Souza', 'socorro.oliveira@ifrn.edu.br'),
('1888424', 'Augusto Cesar de Assis Braga', 'augusto.braga@ifrn.edu.br'),
('1888558', 'Felipe Araujo de Medeiros', 'felipe.araujo@ifrn.edu.br'),
('1894128', 'Marcelo Henrique Ramalho Nobre', 'marcelo.nobre@ifrn.edu.br'),
('1918984', 'Jorge Luiz Ferreira Rabelo', 'jorge.rabelo@ifrn.edu.br'),
('1924598', 'Felipe Garcia de Medeiros', 'felipe.garcia@ifrn.edu.br'),
('1935921', 'Max Miller da Silveira', 'max.silveira@ifrn.edu.br'),
('1940119', 'Alan Jones Lira de Melo', 'alan.melo@ifrn.edu.br'),
('1949000', 'Luis Sergio Dantas da Silva', 'luis.dantas@ifrn.edu.br'),
('1956341', 'Maria das Vitorias Dantas de Gois', 'vitorias.gois@ifrn.edu.br'),
('1957598', 'Luana Carvalho Araujo Pavao', 'luana.carvalho@ifrn.edu.br'),
('1962001', 'Havila Maria Abreu Barbosa', 'havila.barbosa@ifrn.edu.br'),
('1965178', 'Ualison Bezerra Costa Uchoa', 'ualison.uchoa@ifrn.edu.br'),
('1975528', 'Ana Paula Dantas Ferreira', 'ana.dantas@ifrn.edu.br'),
('1996988', 'Joao Carlos Soares de Melo', 'carlos.soares@ifrn.edu.br'),
('1999105', 'Nailton Torres Camara', 'nailton.torres@ifrn.edu.br'),
('2018245', 'Iria Almeida Leal Bassan', 'iria.leal@ifrn.edu.br'),
('2025504', 'Alyne Campelo da Silva', 'alyne.campelo@ifrn.edu.br'),
('2043886', 'Ana Larissa da Silveira', 'larissa.silveira@ifrn.edu.br'),
('2048805', 'Arnobio Bezerra da Silva', 'arnobio.bezerra@ifrn.edu.br'),
('2052482', 'Leila Karyne Torres da Costa', 'leila.karyne@ifrn.edu.br'),
('2055569', 'Francisco das Chagas Souza Junior', 'francisco.souza@ifrn.edu.br'),
('2067162', 'Elias dos Santos Batista', 'Elias.batista@ifrn.edu.br'),
('2068529', 'Patrocina Maria de Queiroz', 'patrocina.queiroz@ifrn.edu.br'),
('2072657', 'Alcione Sayonara de Araujo', 'alcione.araujo@ifrn.edu.br'),
('2082756', 'Marcia Maria Avelino Dantas', 'marcia.dantas@ifrn.edu.br'),
('2108810', 'Karlos Thadeu Matias Vital de Oliveira', 'karlos.oliveira@ifrn.edu.br'),
('2117056', 'Alexandre Vieira Beltrao', 'alexandre.vieira@ifrn.edu.br'),
('2117143', 'Andrio Vitor Martins Rodrigues', 'andrio.rodrigues@ifrn.edu.br'),
('2124992', 'Ruth Medeiros de Oliveira', 'ruth.oliveira@ifrn.edu.br'),
('2126902', 'Lino Araujo Filho', 'lino.filho@ifrn.edu.br'),
('2133191', 'Francisco Aristonio de Almeida Santos', 'aristonio.santos@ifrn.edu.br'),
('2133235', 'Hudson Pablo de Oliveira Bezerra', 'hudson.bezerra@ifrn.edu.br'),
('2145527', 'Jonas Damasce no Batista de Araujo', 'jonas.damasceno@ifrn.edu.br'),
('2151240', 'Talyana Gentil Dias', 'talyana.dias@ifrn.edu.br'),
('2152373', 'Daniela Keller Menezes', 'daniela.menezes@ifrn.edu.br'),
('2167061', 'Luiz Felipe Fernandes Goncalves', 'luiz.goncalves@ifrn.edu.br'),
('2183054', 'Ana Clarissa Bezerra Galvao de Araujo', 'clarissa.galvao@ifrn.edu.br'),
('2210561', 'Sheyla Charlyse Rodrigues de Oliveira', 'sheyla.oliveira@ifrn.edu.br'),
('2211090', 'Alexsandra Fernandes de Queiroz', 'alexsandra.fernandes@ifrn.edu.br'),
('2234319', 'Maria Regia Pereira Miranda', 'maria.regia@ifrn.edu.br'),
('2241439', 'Raquel da Silveira Maia', 'raquel.maia@ifrn.edu.br'),
('2244474', 'Poincyana Sonaly Bessa de Holanda', 'bessa.poincyana@ifrn.edu.br'),
('2247275', 'Maira Dal Maz Pinheiro', 'maira.pinheiro@ifrn.edu.br'),
('2265824', 'Adair Divino Silva Badaro', 'adair.badaro@ifrn.edu.br'),
('2266155', 'Bruno Maia da Costa', 'bruno.maia@ifrn.edu.br'),
('2275854', 'Claudenor Ancelmo da Silva', 'claudenor.silva@ifrn.edu.br'),
('2275872', 'Jardel Lucena da Silva', 'jardel.lucena@ifrn.edu.br'),
('2275904', 'Gabriela Fernandes de Siqueira', 'gabriela.siqueira@ifrn.edu.br'),
('2277237', 'Giulliano Jose Segundo Alves Pereira', 'giulliano.pereira@ifrn.edu.br'),
('2278884', 'Eduardo Augusto Morais Rodrigues', 'eduardo.rodrigues@ifrn.edu.br'),
('2278885', 'Julimar da Silva Goncalves', 'julimar.goncalves@ifrn.edu.br'),
('2278916', 'Livia Juliana Silva Solino de Souza', 'livia.solino@ifrn.edu.br'),
('277352', 'Caubi Ferreira de Souza Junior', 'caubi.junior@ifrn.edu.br'),
('2777919', 'Jose Carlos Vieira de Souza', 'jose.souza@ifrn.edu.br'),
('SIAPE', 'NOME', 'EMAIL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario` varchar(7) NOT NULL,
  `tipo_usuario` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usuario`, `tipo_usuario`) VALUES
('1935921', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_login`
--

CREATE TABLE IF NOT EXISTS `usuario_login` (
  `id` int(11) NOT NULL,
  `usuario` varchar(7) NOT NULL,
  `data_login` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_sistema`
--
ALTER TABLE `login_sistema`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `planejamento`
--
ALTER TABLE `planejamento`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servidor`
--
ALTER TABLE `servidor`
 ADD PRIMARY KEY (`siape`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`usuario`);

--
-- Indexes for table `usuario_login`
--
ALTER TABLE `usuario_login`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_sistema`
--
ALTER TABLE `login_sistema`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `planejamento`
--
ALTER TABLE `planejamento`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
